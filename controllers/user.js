const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if(result.length > 0){
			return true;
		}
		// No duplicate email found
		else {
			return false;
		}
	})
}


// User registration
module.exports.registerUser = (reqBody) => {
	// Creates a new user model
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registration failed
		if(error){
			return false;
		}
		// User registration successful
		else{
			return true;
		}
	})
}

// User Authentication(login)
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false
		}
		// User exists
		{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// If the passwords match/result of the code to true
			if(isPasswordCorrect){
				return { acces : auth.createAccessToken(result)}
			}
			// Passwords do not match
			else{
				return false;
			}
		}
	})
}

// Retrieve user details [s38 Activity]
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};
