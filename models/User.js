const mongoose = require("mongoose");

const userSchema= new mongoose.Schema({
	firstName:{
		type: String,
		required:[true,"firstName is required"]		
	},

    lastName:{
    	type:String,
    	require:[true,"lastName is required"]
    },

    email:{
    	type:String,
    	required:[true,"email is required"]
    },

    password:{
    	type:String,
    	required:[true,"passwrod is required"]
    },

    isAdmin:{
    	type:Boolean,
    	default:false
    },

    mobileNo:{
        type: String,
        required:[true, "mobile No. is required"]
    },

    enrollments:[
    	
        {courseId:{
    		type:String,
    		required:[true, "courseId is required"]
    	},
    	enrolledOn:{
    		type:Date,
    		default: new Date()
    	},
    	status:{
    		type:String,
    		default: "Enrolled"
    	}
    }
    ]
})
module.exports = mongoose.model("User", userSchema);
